import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import NewsPage from "pages/NewsPage";
import NewsDetailPage from "pages/NewsDetailPage";
import NewsForm from "pages/NewsForm";
import FroggyPage from "pages/FroggyPage";
import FroggyPage2 from "pages/FroggyPage2";
import NoMatch from "components/NoMatch";
import store from "./store";

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Routes>
        <Route path="/" element={<NewsPage />} />
        <Route path="/posts" element={<NewsPage />} />
        <Route path="/posts/new" element={<NewsForm />} />
        <Route path="/posts/update" element={<NewsForm />} />
        <Route path="/posts/:id" element={<NewsDetailPage />} />
        <Route path="/games/froggy" element={<FroggyPage />} />
        <Route path="/games/froggy2" element={<FroggyPage2 />} />
        <Route path="*" element={<NoMatch />} />
      </Routes>
    </Router>
  </Provider>,
  document.getElementById("root")
);
