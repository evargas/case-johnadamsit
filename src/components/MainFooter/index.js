import React from 'react'
import styled from "styled-components"
import MainNavigation from 'components/Navigation/MainNavigation'
import { footerLinks } from 'components/Navigation/links'

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`

export default () => (
    <Wrapper>
        <MainNavigation links={footerLinks} submenu footer/>
    </Wrapper>
)