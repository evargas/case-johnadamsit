import React from "react";
import styled from "styled-components"
import MainNavigation from "components/Navigation/MainNavigation";
import SubscribeNavigation from "components/Navigation/SubscribeNavigation";
import Logo from "components/Logo";
import {links, subcriberLinks, subLinks} from "components/Navigation/links"
import { device } from 'helpers'

const CustomHeader = styled.header`
    min-height: 120px;
`

const CustomWraper = styled.div`
    display: flex;
    justify-content: space-between;

    @media ${device.tablet} {
        flex-direction: column;
    }
`

export default () => (
    <CustomHeader>
        <CustomWraper>
            <MainNavigation links={links} uppercase/>
            <SubscribeNavigation links={subcriberLinks}/>
        </CustomWraper>
        <Logo />
        <MainNavigation links={subLinks} submenu/>
    </CustomHeader>
)