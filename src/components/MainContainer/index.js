import React from 'react'
import styled from 'styled-components'
import { device } from 'helpers'

const Wrapper = styled.main`
  box-shadow: 0 0 10px #6a5959;
  width: 920px;
  margin: 0 auto;
  min-heigth: 100vh;

  @media ${device.tablet} {
    width: 100vw;
  }
`

export default ({children}) => (
    <Wrapper>
        {children}
    </Wrapper>
)