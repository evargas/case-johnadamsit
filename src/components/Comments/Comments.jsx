import React, { useEffect } from "react";
import styled from "styled-components";
import { capitalizeParagraph } from "helpers";

const Wrapper = styled.div`
  flex: 1;
  margin: 20px;

  h2 {
    color: #000;
    font-size: 13px;
    font-weight: bolder;
  }

  ul {
    list-style: none;
    padding: 5px 0;

    li {
      border-bottom: 1px solid #ccc;
      margin-top: 10px;

      h3 {
        color: #000;
        font-size: 11px;
        font-weight: bolder;
        text-align: right;
      }

      p {
        text-align: justify;
      }
    }
  }
`;

const Comments = ({ getComments, item, comments }) => {
  useEffect(() => {
    if (item?.id) {
      getComments(item.id);
    }
  }, [item]);
  return (
    <Wrapper>
      <h2>Comments</h2>
      {comments && (
        <ul>
          {comments.map((comment, index) => (
            <li key={index}>
              <h3>{comment.email}</h3>
              <p>{capitalizeParagraph(comment.body)}</p>
            </li>
          ))}
        </ul>
      )}
    </Wrapper>
  );
};

export default Comments