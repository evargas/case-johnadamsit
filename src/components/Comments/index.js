import { connect } from "react-redux";
import {
  getCurrentCommentsSelector,
} from "redux/selectors/newsSelector";
import { getNewsComments } from "redux/services/newsService";
import Comments from "components/Comments/Comments"

const mapStateToProps = (state) => ({
  comments: getCurrentCommentsSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  getComments: (current) => getNewsComments(current, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Comments);
