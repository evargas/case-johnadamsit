import React from 'react'
import styled from "styled-components"

const Wrapper = styled.div`
    background: #e7e7e7;
    padding: 10px 15px;
    display: flex;
    justify-content: space-between;

    input{
        border-color: #e7e7e7;
        flex: 1;
        border-style: none;
        margin-right: 4px;
        height: 30px;
    }
    .btn{
        border-radius: 0px;
        text-transform: capitalize;
        padding: 4px 8px;
        font-size: 11px;
        box-shadow: none;
        border-style: none;
        color: #fff;
        text-decoration: none;
        cursor: pointer;

        &.btn-search{
            background: #964242;
            border: solid 2px darken(#964242, 10%);
            
        }
        
    }
`

export default () => (
    <Wrapper>
        <input type="text" />
        <button type="button" className="btn btn-search">Search</button>
    </Wrapper>
)