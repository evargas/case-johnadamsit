import { connect } from 'react-redux'
import { getNewsImages, getNewsService } from 'redux/services/newsService'
import { getNewsImagesLoadedSelector, getNewsSelector, getCurrentNewsSelector } from 'redux/selectors/newsSelector'
import News from 'components/News/News';

const mapStateToProps = state => ({
    newsList: getNewsSelector(state),
    imagesLoaded: getNewsImagesLoadedSelector(state),
    current: getCurrentNewsSelector(state)
});
  
const mapDispatchToProps = dispatch => ({
    getNews: () => getNewsService(dispatch),
    getNewsImages: (number) => getNewsImages(number, dispatch)
});
  
export default connect(mapStateToProps, mapDispatchToProps)(News)