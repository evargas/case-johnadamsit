import React, { useEffect } from "react";
import styled from "styled-components";
import NewsItem from "components/NewsItem";
import { device } from 'helpers'

const Wrapper = styled.div`
  flex: 2;
  display: flex;
  flex-wrap: wrap;
  padding: 20px;

  @media ${device.tablet} {
    flex-direction: column; 
  }
`;

const News = ({ getNews, newsList, getNewsImages, imagesLoaded, current }) => {
  useEffect(() => {
    if (!current) {
      getNews();
    }
  }, [current]);

  useEffect(() => {
    if (newsList.length > 0 && !imagesLoaded) {
      getNewsImages(newsList.length);
    }
  }, [getNewsImages, newsList, imagesLoaded]);

  return (
    <Wrapper>
      {newsList.map((item, index) => (
        <NewsItem item={item} key={index} />
      ))}
    </Wrapper>
  );
};

export default News;
