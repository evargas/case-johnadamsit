import React from "react";
import styled from "styled-components";
import { connect } from 'react-redux'
import { useNavigate } from "react-router-dom";
import { setNewsCurrent } from "redux/actions/newsAction";
import { getCurrentNewsSelector } from "redux/selectors/newsSelector";
import { capitalizeParagraph } from "helpers";
import popular from "components/Favorites/mockDataPopularList";

const Wrapper = styled.div`
  padding: 10px 15px;

  h3 {
    color: #000;
    font-size: 11px;
    font-weight: bolder;
  }
  ul {
    list-style: none;
    padding-left: 0;

    li {
      border-bottom: 1px solid #aaa;
      height: 16px;
      margin-top: 5px;
      width: 100%;
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
      transition: ease-in-out 0.5s;

      &:hover{
        span{
          color: #000;
        }
      }

      span {
            text-decoration: none;
            color: #888;
            font-size: 11px;
            cursor: pointer;
      }
    }
  }
`;

const Favorites =  ({setCurrent}) => {
  const navigate = useNavigate();
  const handleClick = (item) => {
    if(item?.id){
      navigate(`/posts/${item.id}`);
      setCurrent(item);
    }
  }
  return (
    <Wrapper>
      <h3>POPULAR POST</h3>
      <ul>
        {popular.map((item, index) => (
          <li key={index}>
            <span onClick={() => handleClick(item)}>{capitalizeParagraph(item.title)}</span>
          </li>
        ))}
      </ul>
    </Wrapper>
  )
};

const mapStateToProps = state => ({
  current: getCurrentNewsSelector(state)
});

const mapDispatchToProps = dispatch => ({
  setCurrent: current => setNewsCurrent(current, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Favorites)
