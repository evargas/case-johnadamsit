const popular = [
  {
    id: 1,
    body:'this is the body',
    image:"https://picsum.photos/id/1026/4621/3070",
    title:
      "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
  },
  {
    id: 2,
    body:'this is the body',
    image:"https://picsum.photos/id/1027/2848/4272",
    title: "qui est esse",
  },
  {
    id: 3,
    body:'this is the body',
    image:"https://picsum.photos/id/1028/5184/3456",
    title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
  },
  {
    id: 4,
    body:'this is the body',
    image:"https://picsum.photos/id/1029/4887/2759",
    title: "eum et est occaecati",
  },
  {
    id: 5,
    body:'this is the body',
    image:"https://picsum.photos/id/103/2592/1936",
    title: "nesciunt quas odio",
  },
];

export default popular;
