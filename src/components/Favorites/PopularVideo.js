import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  padding: 10px 15px;

  h3 {
    color: #000;
    font-size: 11px;
    font-weight: bolder;
  }
  
  .video{
    margin-top: 10px;
    width: 100%;
    height: 200px;
  }
`;

export default () => (
  <Wrapper>
    <h3>FEATURED VIDEO</h3>
    <div className="video">
      <iframe width="100%" height="200px" src="https://www.youtube.com/embed/FQLGhPHzxjc" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
    </div>
  </Wrapper>
);
