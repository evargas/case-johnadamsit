import React from 'react'
import styled from "styled-components"
import AdvertiseSquare from 'components/Advertise/AdvertiseSquare'
import SearchInput from 'components/SearchInput'
import Favorites from 'components/Favorites'
import FlickrGalery from 'components/Galery/FlickrGalery'
import PopularVideo from 'components/Favorites/PopularVideo'
import { device } from 'helpers'

const Wrapper = styled.div`
    flex: 1;
    max-width: 280px;
    box-shadow: -8px 0 8px #c8c5c5;

    @media ${device.tablet} {
        max-width: 320px;
        margin: 0 auto;
        box-shadow: none;
    }

`

export default () => (
    <Wrapper>
        <AdvertiseSquare />
        <SearchInput />
        <Favorites />
        <FlickrGalery />
        <PopularVideo />
    </Wrapper>
)