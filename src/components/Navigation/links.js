export const links = [
    {
        label: 'Home',
        url: '/'
    },
    {
        label: 'About Us',
        url: '/about-us'
    },
    {
        label: 'Demo Page',
        url: '/demo-page'
    },
    {
        label: 'Contact Page',
        url: '/contact-page'
    },
]

export const subLinks = [
    {
        label: 'Advertising',
        url: '/advertising'
    },
    {
        label: 'Entertainment',
        url: '/entertainment'
    },
    {
        label: 'Fashion',
        url: '/fashion'
    },
    {
        label: 'Lifestyle',
        url: '/lifestyle'
    },
    {
        label: 'Pictures',
        url: '/pictures'
    },
    {
        label: 'Videos',
        url: '/videos'
    },
    {
        label: 'Games',
        url: '/games/froggy2'
    },
    {
        label: 'New Post',
        url: '/posts/new'
    },
]

export const subcriberLinks = [
    {
        label: 'Posts',
        url: '/posts'
    },
    {
        label: 'Comments',
        url: '/comments'
    },
    {
        label: 'email',
        url: '/email'
    }
]

export const footerLinks = [
    {
        label: 'Erick Vargas 2021',
        url: '/https://www.linkedin.com/in/vargas-erick-1ab86b102/'
    }
]