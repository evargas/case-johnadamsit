import React from "react";
import styled from "styled-components"
import MainNavigation from "components/Navigation/MainNavigation";
import { device } from 'helpers'

const Wrapper = styled.div`
    display: flex;
    justify-content: start;
    align-items: center;

    @media ${device.tablet} {
        margin-left: 10px;
    }

    span{
        font-size: 12px;
        margin-top: 2px;
    }
`

export default ({links}) => (

    <Wrapper>
        <span>Subscribe: </span>
        <MainNavigation links={links}/>
    </Wrapper>
)