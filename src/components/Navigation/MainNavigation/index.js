import React from "react";
import styled from "styled-components"
import { Link } from "react-router-dom";
import { device } from 'helpers'

const CustomList = styled.ul`
    display: flex;
    list-style: none;
    background: ${props => (props.submenu ? '#252525' : '#fff')};
    padding: ${props => (props.submenu ? '2px 0' : '0px')};
    width: 100%;
    justify-content: ${props => (props.footer ? 'center' : 'flex-start')};

    @media ${device.tablet} {
        flex-wrap: ${props => (props.submenu ? 'wrap' : 'no-wrap')}; 
        max-width: 100vw;
        justify-content: ${props => (props.submenu ? 'center' : 'start')};
    }

    li{
        padding: 5px 15px;
        position: relative;

        @media ${device.tablet} {
            padding: ${props => (props.uppercase ? '5px 9px' : '5px 15px')};
        }
        
        a{
            text-decoration: none;
            color: ${props => (props.submenu ? '#fff' : '#964242')};
            font-weight: ${props => (props.submenu ? 'normal' : 'bold')};
            font-size: 11px;
            text-transform: ${props => (props.uppercase ? 'uppercase' : 'default')};
        }

        &:after {
            content: "";
            position: absolute;
            left: 0;
            width: 1px;
            height: 12px;
            background: #252525;
            top: 9px;
        }

        &:first-child {
            &:after {
                background: ${props => (props.submenu ? '#252525' : '#fff')};;
            }
        }
    }
`

export default ({links, uppercase, submenu, footer}) => (
    <CustomList uppercase={uppercase} submenu={submenu} footer={footer}>
        {
            links.map((link, index) =><li key={index}>
                <Link to={link.url}>{link.label}</Link>
            </li>)
        }
    </CustomList>
)
