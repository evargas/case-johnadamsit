import React from "react";
import styled from "styled-components"

export const Container = styled.div`
    width: 100%;
    height: 100%;
    background: black;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    box-sizing: border-box;

    .block{
        min-height: 100%;
        flex: 1;
        padding: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    
    .board{
        display: flex;
        justify-content: center;
        align-items: center;
        width: auto;
        flex-wrap: wrap;
        max-width: 400px;
        position: relative;
    }
    
    .cell{
        width: 100px;
        height: 100px;
        background: skyblue;
        border: 1px solid black;
        box-sizing: border-box;
    }
    
    .cell-x{
        position: absolute;
        background-color: blue;
        bottom: 0;
        left: 0;
        z-index: 1005;
        transition: ease-in-out 0.5s;
    }
    
    .block-right .board{
        transform: rotate(45deg);
        max-width: 280px;
        border: 1px solid transparent;
    }
    
    .block-right .cell{
        margin: 10px;
        cursor: pointer;
    }
    
    .block-right .cell.cell-up{
        background-color:green;
    }
    .block-right .cell.cell-right{
        background-color:red;
    }
    .block-right .cell.cell-down{
        background-color:skyblue;
    }
    .block-right .cell.cell-left{
        background-color:orange;
    }
`

const moveUp = () => {
    var elem = document.getElementById('cell-x');
    var position = parseInt(window.getComputedStyle(elem).bottom);
    if(position < 300){
        var sum = position + 100;
        elem.style.bottom = sum + 'px';
    }
}

const moveDown = () => {
    var elem = document.getElementById('cell-x');
    var position = parseInt(window.getComputedStyle(elem).bottom);
    if(position > 0){
        var sum = position - 100;
        elem.style.bottom = sum + 'px';
    }
}

const moveRight = () => {
    var elem = document.getElementById('cell-x');
    var position = parseInt(window.getComputedStyle(elem).left);
    if(position < 300){
        var sum = position + 100;
        elem.style.left = sum + 'px';
    }
}

const moveLeft = () => {
    var elem = document.getElementById('cell-x');
    var position = parseInt(window.getComputedStyle(elem).left);
    if(position > 0){
        var sum = position - 100;
        elem.style.left = sum + 'px';
    }
}



export default () => (
    <Container>
        <div class="block block-left">
            <div class="board">
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell"></div>
                <div class="cell cell-x" id="cell-x"></div>
            </div>
        </div>
        <div class="block block-right">
            <div class="board">
                <div class="cell cell-up" onClick={() => moveUp()}></div>
                <div class="cell cell-right" onClick={() => moveRight()}></div>
                <div class="cell cell-left" onClick={() => moveLeft()}></div>
                <div class="cell cell-down" onClick={() => moveDown()}></div>
            </div>
        </div>
    </Container>
)