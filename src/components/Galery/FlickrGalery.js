import React from "react";
import styled from "styled-components";
import mockData from "components/Galery/mockDataFlickr";

const Container = styled.div`
  padding: 10px 15px;

  h3 {
    color: #000;
    font-size: 11px;
    font-weight: bolder;
  }
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  margin: 10px 0;
`;
const Square = styled.div`
  height: 75px;
  width: 75px;
  border: solid 1px #aaa;
  margin: 3px;
  background-image: url(${(props) => props.item.url});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  transition: ease-in-out 0.5s;

  &:hover {
    transform: scale(1.1);
  }
`;

export default () => (
  <Container>
    <h3>FLICKR PHOTOS</h3>
    <Wrapper>
      {mockData.map((item, index) => (
        <a key={index} href={item.link}>
          <Square item={item} key={index} />
        </a>
      ))}
    </Wrapper>
  </Container>
);
