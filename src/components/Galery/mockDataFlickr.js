const mockData = [
    {
      link: "https://www.flickr.com/photos/bygeorge21/51335134556/in/photolist-4kgYBR-5KYyst-xrxzx-4Ji1re-6uYnYT-7LLC8Q-MLwsv-7Tm9R4-eCpWn-4tHp8E-39wJwG-7ZgoCn-4LCLKP-2kug4z8-4XHKpZ-9Ptm1M-5kVQtk-6H8fsD-4WvPMr-92g1TR-8jnX4b-8jnQLC-6UeWs8-8jjBCT-8jjtAM-5tHMB4-236Zf6c-L5dycN-F9fSqy-2msnyWN-Ppgcs9-Mz8qre-Gx26JD-JMqa16-2mQkKMW-732b8h-24ED6R1-DYeRCL-2kr37wy-2mFi6dh-2mFhNF9-CnbvgY-2mdiZ1Q-73PXbD-PxBi9N-2hLiQGh-251ZPoR-2k5Cy8G-2kzBLgc-2kD1Xdo",
      url: "https://live.staticflickr.com/65535/51335134556_12155d7038_b.jpg",
    },
    {
      link: "https://www.flickr.com/photos/130488194@N03/51755264875/in/photolist-2mRNLBf-2mTiqh6-2mRXhKt-2mQTHYR-2mQmbbB-2mRrg82-2mSHfJ9-2mPWhRd-2mPHiFH-2mPp18x-2mPRoo1-2mQbAqb-2mNrbED-2mRhfYn-2mRzzxr-2mSm91N-2mRyZx9-2mRpXqK-2mS8Ssj-2mN517h-2mTny5n-2mQnHid-2mMVLtK-2mPvBMo-2mMkac7-2mRgVVk-2mTesKX-2mSwT7j-2mRYmPb-2mParQE-2mMLKdz-2mNxCG2-2mNft4X-2mQmABQ-2mPGtGo-2mRFR5M-2mSXZP5-2mMNTZo-2mQNqmL-2mRcXCD-2mPdbLq-2mNT5s6-2mReLbY-2mQpT8H-2mNojLs-2mR6VPx-2mR6XHs-2mMFmmb-2mRyJkX-2mP1vsC",
      url: "https://live.staticflickr.com/65535/51755264875_d569f28277_h.jpg",
    },
    {
      link: "https://www.flickr.com/photos/naturalworldfotos/51731025829/in/photolist-2mRigym-2mSvhym-2mRp8nr-2mQBuTs-2mPi2Gr-2mNv4Wc-2mP7QjW-2mNGgSX-2mPEhm8-2mNo8ua-2mRjPBw-2mQgGBK-2mPmhYF-2mNqKbn-2mPrYuP-2mRW4LN-2mNTz3T-2mTaNvg-2mR5nME-2mPYsj1-2mSkuR5-2mReJ8y-2mQTZE5-2mRmfcc-2mQ4a3H-2mSWFfm-2mRCDWJ-2mRiLdc-2mSSgrz-2mQmBDt-2mT27n9-2mT6EfW-2mQfaW3-2mNBkhf-2mQ7eUt-2mTk1yM-2mRurGa-2mSexcP-2mSJcuK-2mSWNN3-2mN6rXL-2mPRs5T-2mRzomx-2mPumuf-2mPTDwt-2mQrcsP-2mPKMwx-2mPpcJH-2mQbwPu-2mNxeu2",
      url: "https://live.staticflickr.com/65535/51731025829_787bb19026_h.jpg",
    },
    {
      link: "https://www.flickr.com/photos/greenfields/51753705448/in/photolist-2mRigym-2mSvhym-2mRp8nr-2mQBuTs-2mPi2Gr-2mNv4Wc-2mP7QjW-2mNGgSX-2mPEhm8-2mNo8ua-2mRjPBw-2mQgGBK-2mPmhYF-2mNqKbn-2mPrYuP-2mRW4LN-2mNTz3T-2mTaNvg-2mR5nME-2mPYsj1-2mSkuR5-2mReJ8y-2mQTZE5-2mRmfcc-2mQ4a3H-2mSWFfm-2mRCDWJ-2mRiLdc-2mSSgrz-2mQmBDt-2mT27n9-2mT6EfW-2mQfaW3-2mNBkhf-2mQ7eUt-2mTk1yM-2mRurGa-2mSexcP-2mSJcuK-2mSWNN3-2mN6rXL-2mPRs5T-2mRzomx-2mPumuf-2mPTDwt-2mQrcsP-2mPKMwx-2mPpcJH-2mQbwPu-2mNxeu2",
      url: "https://live.staticflickr.com/65535/51753705448_39b5018796_h.jpg",
    },
    {
      link: "https://www.flickr.com/photos/the_crow4/50763720126/in/photolist-Qrh9KR-LWG7Kv-KvtFMM-DHWaaj-Kmbdac-2kkPkpU-26D1JEY-Qk6S4v-NagNrD-2mACuTL-2kWxC9X-zZg4sS-JTJZdz-HMGhBS-2kJp21r-Re7J2V-Jc5n6U-SE7PtW-RpMYBc-2kiRdjq-EUrFXc-2mJ6DrY-2mygVgJ-2kK9QxU-2kyBmnD-ALjBx-2mCVT4r-GqvSJP-2k1FpXM-FuNnrq-2meCS3G-2kmprx3-2kThHhW-2mpo4Qy-2k9MpeC-2bTMqc2-2mirMbC-2kuMWSP-2kZVwSo-2kwTsRe-2hpvYkU-Ric2yX-2kwXbu9-2kBamub-Pd2k4z-2mAzUvA-aCR61w-2kHuPx7-2m3tWVm-2myMfyf",
      url: "https://live.staticflickr.com/65535/50763720126_04e763b664_h.jpg",
    },
    {
      link: "https://www.flickr.com/photos/183893986@N07/51587709994/in/photolist-Qrh9KR-LWG7Kv-KvtFMM-DHWaaj-Kmbdac-2kkPkpU-26D1JEY-Qk6S4v-NagNrD-2mACuTL-2kWxC9X-zZg4sS-JTJZdz-HMGhBS-2kJp21r-Re7J2V-Jc5n6U-SE7PtW-RpMYBc-2kiRdjq-EUrFXc-2mJ6DrY-2mygVgJ-2kK9QxU-2kyBmnD-ALjBx-2mCVT4r-GqvSJP-2k1FpXM-FuNnrq-2meCS3G-2kmprx3-2kThHhW-2mpo4Qy-2k9MpeC-2bTMqc2-2mirMbC-2kuMWSP-2kZVwSo-2kwTsRe-2hpvYkU-Ric2yX-2kwXbu9-2kBamub-Pd2k4z-2mAzUvA-aCR61w-2kHuPx7-2m3tWVm-2myMfyf",
      url: "https://live.staticflickr.com/65535/51587709994_5bd7022ad8_h.jpg",
    },
  ];

  export default mockData