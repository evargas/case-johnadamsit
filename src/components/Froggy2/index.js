import React, {useState, useRef, useEffect} from "react";
import styled from "styled-components"

export const Container = styled.div`
    width: 100%;
    height: 100%;
    background: black;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    box-sizing: border-box;

    .block{
        min-height: 100%;
        flex: 1;
        padding: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    
    .board{
        display: flex;
        justify-content: center;
        align-items: center;
        width: auto;
        flex-wrap: wrap;
        max-width: 400px;
        position: relative;
    }
    
    .cell{
        width: 100px;
        height: 100px;
        background: skyblue;
        border: 1px solid black;
        box-sizing: border-box;
    }
    
    .cell-x{
        position: absolute;
        background-color: blue;
        bottom: ${props => props.position[0]}px;
        left: ${props => props.position[1]}px;
        z-index: 1005;
        transition: ease-in-out 0.5s;
    }
    
    .block-right .board{
        transform: rotate(45deg);
        max-width: 280px;
        border: 1px solid transparent;
    }
    
    .block-right .cell{
        margin: 10px;
        cursor: pointer;
    }
    
    .block-right .cell.cell-up{
        background-color:green;
    }
    .block-right .cell.cell-right{
        background-color:red;
    }
    .block-right .cell.cell-down{
        background-color:skyblue;
    }
    .block-right .cell.cell-left{
        background-color:orange;
    }
`

export default () => {

    const ref = useRef()
    
    const [position, setPosition] = useState([300, 300]);
    const cells = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

    const moveUp = () => {
        if(position[0] < 300){
            setPosition((prevState) => [prevState[0] + 100, prevState[1]])
        }
    }
    
    const moveDown = () => {
        if(position[0] > 0){
            setPosition((prevState) => [prevState[0] - 100, prevState[1]])
        }
    }
    
    const moveRight = () => {
        if(position[1] < 300){
            setPosition((prevState) => [prevState[0], prevState[1] + 100])
        }
    }
    
    const moveLeft = () => {
        if(position[1] > 0){
            setPosition((prevState) => [prevState[0], prevState[1] - 100])
        }
    }

    const handleControls = (e) => {
        switch (e.code) {
            case 'ArrowUp':
                moveUp()
                break;
            case 'ArrowRight':
                moveRight()
                break;
            case 'ArrowLeft':
                moveLeft()
                break;
            case 'ArrowDown':
                moveDown()
                break;
        
            default:
                break;
        }
    }

    useEffect(() => {
        ref.current.focus()
    })

    return (
        <Container position={position} onKeyDown={handleControls} tabIndex={0} ref={ref}>
            <div className="block">
                <div className="board">
                    {
                        cells.map((item) => <div key={item} className="cell" />)
                    }
                    <div className="cell cell-x" />
                </div>
            </div>
            <div className="block block-right">
                <div className="board">
                    <div className="cell cell-up" onClick={() => moveUp()} />
                    <div className="cell cell-right" onClick={() => moveRight()} />
                    <div className="cell cell-left" onClick={() => moveLeft()} />
                    <div className="cell cell-down" onClick={() => moveDown()} />
                </div>
            </div>
        </Container>
    )
}