import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  margin: 10px 0;
`;
const Square = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 120px;
  width: 120px;
  border: dashed 1px #aaa;
  background: #e7e7e7;
  color: #888;
  font-size: 12px;
  font-weight: bold;
  margin: 3px;

  .text {
    font-size: 13px;
  }
  .mesures {
    font-size: 25px;
  }
`;

const advertise = [0, 1, 2, 3];

export default () => (
  <Wrapper>
    {advertise.map((adv, index) => (
      <Square key={index}>
        <span className="text">Advertise Here</span>
        <span className="mesures">125x125</span>
      </Square>
    ))}
  </Wrapper>
);
