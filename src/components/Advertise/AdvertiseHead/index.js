import React from 'react'
import styled from 'styled-components'
import { device } from 'helpers'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 55px;
  width: 420px;
  border: solid 1px #ccc;
  background: #e7e7e7;
  color: #888;
  font-size: 12px;
  font-weight: bold;
  margin-right: 3px;

  @media ${device.tablet} {
      display: none;
  }
`

export default () => (
    <Wrapper>
        Advertise Here
    </Wrapper>
)