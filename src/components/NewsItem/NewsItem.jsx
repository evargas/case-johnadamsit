import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import { capitalizeParagraph } from "helpers";
import Comments from "components/Comments";

const Wrapper = styled.div`
  flex: 1;
  min-width: calc(50% - 50px);
  margin-bottom: 40px;
  padding: 10px 25px;
  transition: ease-in-out 0.5s;

  &:hover {
    background: ${(props) => (props.detail ? "" : "#ddd")};

    .item-image {
      filter: ${(props) => (props.detail ? "" : "grayscale(1)")};
    }
  }

  .item-image {
    border: solid #eee 4px;
    width: 100%;
    min-height: ${(props) => (props.detail ? "300px" : "80px")};
    background-image: url(${(props) =>
      props?.item?.image ? props.item.image : ""});
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
    transition: ease-in-out 0.5s;
  }
  h1,
  h2 {
    font-size: 16px;
    color: #000;
    padding: 8px 0;
    font-family: "Fraunces", serif;
  }
  span {
    font-size: 10px;
    color: #aaa;
  }
  p {
    font-size: 11px;
    color: #999;
    padding: 5px 0;
    line-height: 13px;
  }

  .actions {
    display: flex;
    justify-content: ${(props) =>
      props.detail ? "flex-end" : "space-between"};

    .btn {
      border-radius: 0px;
      text-transform: capitalize;
      padding: 6px 8px;
      font-size: 11px;
      box-shadow: none;
      border-style: none;
      color: #fff;
      text-decoration: none;
      margin-left: ${(props) => (props.detail ? "5px" : "0")};
      cursor: pointer;

      &.btn-more {
        background: #964242;
        border: solid 2px darken(#964242, 10%);
      }
      &.btn-update {
        background: orange;
        border: solid 2px darken(orange, 10%);
      }
      &.btn-delete {
        background: #d61206;
        border: solid 2px darken(#d61206, 10%);
      }
    }
  }
`;

const NewsItem = ({ detail, item, setCurrent, current, deletePost, updatePost }) => {
  const [currentItem, setCurrentItem] = useState(item);
  const navigate = useNavigate();

  const handleSeeMore = (item) => {
    if(item?.id){
      navigate(`/posts/${item.id}`);
      setCurrent(item);
    }
  };

  const handleBack = () => {
    navigate(`/posts`);
  };

  const handleDelete = (item) => {
    if(item?.id){
      deletePost(item.id)
    }
  }
  
  const handleUpdate = (item) => {
    if(item?.id){
      navigate(`/posts/update`);
      setCurrent(item);
    }
  }

  useEffect(() => {
    if (detail) {
      if(!current){
        navigate(`/posts`);
      }else{
        setCurrentItem(current);
      }
    }
  }, [detail, setCurrentItem, current]);

  useEffect(() => {
    if (!detail) {
      setCurrentItem(item);
    }
  }, [detail, setCurrentItem, item]);

  return (
    <Wrapper item={currentItem} detail={detail}>
      <div className="item-image"></div>
      {!detail ? (
        <h2>{currentItem ? capitalizeParagraph(currentItem.title) : "--"}</h2>
      ) : (
        <h1>{currentItem ? capitalizeParagraph(currentItem.title) : "--"}</h1>
      )}
      <span>Posted on January 7, 2008 by Admin</span>
      <p>{currentItem ? capitalizeParagraph(currentItem.body) : "--"}</p>
      <div className="actions">
        {!detail && (
          <button
            onClick={() => handleSeeMore(currentItem)}
            className="btn btn-more"
          >
            Continue reading
          </button>
        )}
        {detail && (
          <button onClick={() => handleBack()} className="btn btn-more">
            Go back
          </button>
        )}
        <button onClick={() => handleUpdate(currentItem)} className="btn btn-update">Update</button>
        <button onClick={() => handleDelete(currentItem)} className="btn btn-delete">Delete</button>
      </div>
      {detail && <Comments item={currentItem} />}
    </Wrapper>
  );
};

export default NewsItem;
