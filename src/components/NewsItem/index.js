import NewsItem from "./NewsItem"
import { connect } from 'react-redux'
import { setNewsCurrent } from "redux/actions/newsAction";
import { getCurrentNewsSelector } from "redux/selectors/newsSelector";
import { updatePost, deletePost } from "redux/services/newsService";

const mapStateToProps = state => ({
  current: getCurrentNewsSelector(state)
});

const mapDispatchToProps = dispatch => ({
  setCurrent: current => setNewsCurrent(current, dispatch),
  updatePost: postId => updatePost(postId, dispatch),
  deletePost: postId => deletePost(postId, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsItem)