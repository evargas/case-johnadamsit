import React from 'react'
import SideBar from 'components/SideBar'
import styled from "styled-components"


const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`

export default ({children}) => {
  return (
      <Wrapper>
          {children}
          <SideBar />
      </Wrapper>
  )
}