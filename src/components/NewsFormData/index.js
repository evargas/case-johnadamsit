import { connect } from 'react-redux'
import { getCurrentNewsSelector, getRequest } from "redux/selectors/newsSelector";
import { updatePost, savePost } from "redux/services/newsService";
import NewsFormData from "./NewsFormData"

const mapStateToProps = state => ({
  current: getCurrentNewsSelector(state),
  request: getRequest(state),
});

const mapDispatchToProps = dispatch => ({
  updatePost: post => updatePost(post, dispatch),
  savePost: post => savePost(post, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsFormData)