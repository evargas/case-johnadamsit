import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import users from "helpers/users";

const Wrapper = styled.main`
  flex: 2;
  padding: 20px;
  background: #e7e7e7;
  padding: 10px 15px;

  h2 {
    font-size: 16px;
    color: #000;
    padding: 25px 0;
    font-family: "Fraunces", serif;
    text-align: center;
  }
  .error{
    color: #fff;
    background: #888;
    padding: 10px;
    text-align: center;
    margin: 0 auto;
    width: 200px;
    font-size: 12px;
    border-radius: 5px;
    boder: solid 2px #fff;
  }

  .inputs-wrapper {
    max-width: 300px;
    margin: 0 auto;
    display: flex;
    flex-wrap: wrap;
    input,
    select {
      border-color: #e7e7e7;
      flex: 1;
      border-style: none;
      margin-right: 4px;
      height: 30px;
      width: auto;
      margin-bottom: 5px;
      padding: 0 10px;
    }
    .btn {
      flex: 1;
      border-radius: 0px;
      text-transform: capitalize;
      padding: 4px 8px;
      font-size: 11px;
      box-shadow: none;
      border-style: none;
      color: #fff;
      text-decoration: none;
      height: 30px;
      cursor: pointer;
      margin-right: 4px;

      &.btn-primary {
        background: #964242;
        border: solid 2px darken(#964242, 10%);
      }
      &.btn-secondary {
        background: #d61206;
        border: solid 2px darken(#d61206, 10%);
      }
    }
  }
`;

export default ({updatePost, savePost, current, request}) => {
  const navigate = useNavigate();
  const initialState = {
    title: "",
    body: "",
    id: "",
    userId: 11,
  };

  const { sending, response } = request

  const [form, setForm] = useState(initialState);
  const [error, setError] = useState(false);

  const handleChangeInput = (e) => {
    const name = e.target.name
    const value = e.target.value
    setForm({
      ...form,
      [name]: value
    })
  }

  const handleSendForm = () => {
    var error = false
    const { title, body, userId } = form;
    if(title === '' || body === '' || userId === 0){
      error  = true
    }

    if(!error){
      if(form.id){
        updatePost(form)
      }else{
        savePost(form)
      }
      setError(false)
    }else{
      setError(true)
    }

  }

  const handleBack = () => {
    navigate(`/posts`);
  };

  useEffect(() =>{
    if(current){
      setForm(current)
    }
  }, [current])

  const { title, body, userId } = form;
  return (
    <Wrapper>
      <h2>{current ? 'Update' : 'New'} Post</h2>
      <div className="inputs-wrapper">
        <input
          type="text"
          name="title"
          placeholder="Write a title..."
          value={title}
          onChange={(e) => handleChangeInput(e)}
        />
        <input
          type="text"
          name="body"
          placeholder="Write a content..."
          value={body}
          onChange={(e) => handleChangeInput(e)}
        />

        <select name="userId" value={userId} onChange={(e) => handleChangeInput(e)}>
          <option value="0">Select the User</option>
          {users.map((user, index) => (
            <option key={index} value={user.id}>
              {user.name}
            </option>
          ))}
          <option value="11">Erick Vargas</option>
        </select>
        <button onClick={() => handleSendForm()} className="btn btn-primary">Save</button>
        <button onClick={() => handleBack()} className="btn btn-secondary">Back</button>
      </div>
        {
          error && <h3 className="error">You must fill all the inputs</h3>
        }
        {
          sending && <h3 className="error">Sending data...</h3>
        }
        {
          !sending && response && <h3 className="error">{`Saved ok with the ID: ${response.id}`}</h3>
        }
    </Wrapper>
  );
};
