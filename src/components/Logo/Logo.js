import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 15px;

  span{
    font-family: 'Fraunces', serif;
    font-size: 26px;
    color: #252525;
    text-transform: uppercase;
  }

  h2{
    color: #964242;
    font-weight: bold;
    font-size: 14px;
    letter-spacing: -1px;
    margin-left: 12px
  }
`

export default () => (
    <Wrapper>
        <span>The Web News</span>
        <h2>Free CSS Template</h2>
    </Wrapper>
)