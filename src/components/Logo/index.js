import React from "react";
import styled from "styled-components";
import Logo from "components/Logo/Logo";
import AdvertiseHead from "components/Advertise/AdvertiseHead";
import { device } from 'helpers'

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 20px 5px;
  
  @media ${device.tablet} {
    flex-direction: column; 
  }
`;

export default () => (
  <Wrapper>
    <Logo />
    <AdvertiseHead />
  </Wrapper>
);
