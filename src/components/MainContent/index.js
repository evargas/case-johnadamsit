import React from 'react'
import NewsList from 'components/News'
import SideBar from 'components/SideBar'
import styled from "styled-components"
import NewsItem from 'components/NewsItem'


const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`

export default ({detail}) => {
  return (
      <Wrapper>
          {
            !detail ? 
            <NewsList />:
            <NewsItem detail/>
          }
          <SideBar />
      </Wrapper>
  )
}