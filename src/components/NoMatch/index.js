import React from "react";
import { useLocation } from "react-router-dom";
import MainHeader from "components/Header/MainHeader";
import MainContainer from "components/MainContainer";
import MainFooter from "components/MainFooter";
import styled from "styled-components";

const Wrapper = styled.div`
    h3{
        color: #964242;
        font-size: 30px;
        margin: 30px;
    }
`

export default () => {
  let location = useLocation();

  return (
    <MainContainer>
      <MainHeader />
      <Wrapper>
        <h3>
          No match for <code>{location.pathname}</code>
        </h3>
      </Wrapper>
      <MainFooter />
    </MainContainer>
  );
};
