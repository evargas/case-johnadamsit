import React from "react";
import MainHeader from "components/Header/MainHeader";
import MainContainer from "components/MainContainer";
import MainContent from "components/MainContent";
import MainFooter from "components/MainFooter";

export default () => (
    <MainContainer>
        <MainHeader />
        <MainContent />
        <MainFooter />
    </MainContainer>
)