import React from "react";
import MainHeader from "components/Header/MainHeader";
import MainContainer from "components/MainContainer";
import MainFooter from "components/MainFooter";
import MainTemplate from "components/MainTemplate";
import NewsFormData from "components/NewsFormData";

export default () => (
    <MainContainer>
        <MainHeader />
        <MainTemplate>
            <NewsFormData />
        </MainTemplate>
        <MainFooter />
    </MainContainer>
)