import React from "react";
import MainHeader from "components/Header/MainHeader";
import MainContainer from "components/MainContainer";
import MainFooter from "components/MainFooter";
import Froggy from "components/Froggy2";

export default () => (
    <MainContainer>
        <MainHeader />
        <Froggy />
        <MainFooter />
    </MainContainer>
)