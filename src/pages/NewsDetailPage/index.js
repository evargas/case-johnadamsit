import React from "react";
import MainHeader from "components/Header/MainHeader";
import MainContainer from "components/MainContainer";
import MainFooter from "components/MainFooter";
import MainContent from "components/MainContent";

export default () => (
    <MainContainer>
        <MainHeader />
        <MainContent detail />
        <MainFooter />
    </MainContainer>
)