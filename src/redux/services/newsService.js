import { getRandomInt } from "helpers";
import {
  setNews,
  setNewsImages,
  setNewsComments,
  setUsers,
  setRequest
} from "../actions/newsAction";

export const getNewsService = (dispatch) => {
  fetch("https://jsonplaceholder.typicode.com/posts")
    .then((response) => response.json())
    .then((users) => {
      const init = getRandomInt(90);
      const usersTruncated = users.slice(init, init + 10);
      setNews(usersTruncated, dispatch);
    });
};

export const getNewsImages = (number, dispatch) => {
  const page = getRandomInt(10);
  fetch(`https://picsum.photos/v2/list?page=${page}&limit=${number}`)
    .then((response) => response.json())
    .then((images) => setNewsImages(images, dispatch));
};

export const getNewsComments = (postId, dispatch) => {
  fetch(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`)
    .then((response) => response.json())
    .then((comments) => setNewsComments(comments, dispatch));
};

export const getUserById = (userId, dispatch) => {
  fetch(`https://jsonplaceholder.typicode.com/users?id=${userId}`)
    .then((response) => response.json())
    .then((users) => setUsers(users, dispatch));
};

export const savePost = (post, dispatch) => {
  setRequest({
    sending: true,
    type: 'POST',
    response: null,
  }, dispatch)
  fetch(`https://jsonplaceholder.typicode.com/posts/${post.id}`, {
    method: "POST",
    body: JSON.stringify(post),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((posts) => {
      setRequest({
        sending: false,
        type: 'POST',
        response: posts
      }, dispatch)

      setTimeout(()=>setRequest({
        sending: false,
        type: 'POST',
        response: null
      }, dispatch), 5000)
    });
};

export const updatePost = (post, dispatch) => {
  setRequest({
    sending: true,
    type: 'PUT',
    response: null,
  }, dispatch)
  fetch(
    `https://jsonplaceholder.typicode.com/posts/${post.id}`,
    {
      method: "PUT",
      body: JSON.stringify(post),
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
    .then((response) => response.json())
    .then((posts) => {
      setRequest({
        sending: false,
        type: 'PUT',
        response: posts
      }, dispatch)

      setTimeout(()=>setRequest({
        sending: false,
        type: 'PUT',
        response: null
      }, dispatch), 5000)
    });
};

export const deletePost = (postId, dispatch) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
    method: "DELETE",
  })
    .then((response) => response.json())
    .then((posts) => console.log(posts));
};
