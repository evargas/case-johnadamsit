export const setNews = (news, dispatch) => {
  dispatch({
    type: "SET_NEWS",
    news
  });
};

export const setNewsImages = (images, dispatch) => {
  dispatch({
    type: "SET_NEWS_IMAGES",
    images
  });
};

export const setNewsError = (error, dispatch) => {
  dispatch({
    type: "SET_NEWS_ERROR",
    error
  });
};

export const setNewsLoading = (loading, dispatch) => {
  dispatch({
    type: "SET_NEWS_LOADING",
    loading
  });
};

export const setNewsImagesLoading = (loading, dispatch) => {
  dispatch({
    type: "SET_NEWS_IMAGES_LOADING",
    loading
  });
};

export const setNewsCurrent = (current, dispatch) => {
  dispatch({
    type: "SET_CURRENT_NEWS",
    current
  });
};

export const setNewsComments = (comments, dispatch) => {
  dispatch({
    type: "SET_CURRENT_COMMENTS",
    comments
  });
};

export const setUsers = (users, dispatch) => {
  dispatch({
    type: "SET_USERS",
    users
  });
};

export const setRequest = (request, dispatch) => {
  dispatch({
    type: "SET_REQUEST",
    request
  });
};

export const clearStateNews = (dispatch) => {
  dispatch({
    type: "CLEAR_NEWS",
  });
};
