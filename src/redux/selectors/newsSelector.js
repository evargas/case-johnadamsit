import get from "lodash/get";
import { createSelector } from "reselect";

export const getNewsSelector = store => get(store, "news.list", []);
export const getNewsImagesSelector = store => get(store, "news.images", []);
export const getNewsErrorSelector = store => get(store, "news.error", null);
export const getNewsLoadingSelector = store => get(store, "news.loading", null);
export const getNewsImagesLoadedSelector = store => get(store, "news.imagesLoaded", false);
export const getCurrentNewsSelector = store => get(store, "news.current", null);
export const getCurrentCommentsSelector = store => get(store, "news.comments", []);
export const getRequest = store => get(store, "news.request", {});
// export const getUserNameByIdSelector = store => get(usersId, store, `news.users[${userId}]`, []);

export const countNews = createSelector(getNewsSelector, news => news.length);

export const getFullNews = createSelector(getNewsSelector, news => news)