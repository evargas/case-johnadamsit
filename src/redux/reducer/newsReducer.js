const stateDefault = {
  list: [],
  error: null,
  loading: false,
  imagesLoaded: false,
  current: null,
  comments: [],
  users: {}, 
  request: {
    sending: false,
    type: '',
    response: null
  },
}

export default (state = stateDefault, action) => {
  switch (action.type) {
    case "SET_NEWS":
      return {
        ...state,
        list: action.news,
        imagesLoaded: false,
      };
    case "SET_NEWS_IMAGES":
      return {
        ...state,
        list: state.list.map((item, index) =>({...item, image: action.images[index].download_url})),
        imagesLoaded: true,
      };
    case "SET_NEWS_ERROR":
      return {
        ...state,
        error: action.error
      };
    case "SET_NEWS_LOADING":
      return {
        ...state,
        loading: action.loading
      };
    case "SET_CURRENT_NEWS":
      return {
        ...state,
        current: action.current
      };
    case "SET_CURRENT_COMMENTS":
      return {
        ...state,
        comments: action.comments
      };
    case "SET_USERS":
      return {
        ...state,
        users: action.users
      };
    case "SET_REQUEST":
      return {
        ...state,
        request: action.request
      };
    case "CLEAR_NEWS":
      return stateDefault;

    default:
      break;
  }

  return state;
};
